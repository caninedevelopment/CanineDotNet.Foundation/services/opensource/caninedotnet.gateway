# Introduction

PLEASE WRITE AN INTRODUCTION TO YOUR SERVICE HERE.

## Purpose

PLEASE WRITE THE PURPOSE OF YOUR SERVICE

## Envoriment settings
gateway_cors				: comma (,) seperated list of whitelisted hostnames used for CORS
gateway_internalservername	: the internal servername used for side-channel requests from services to this gateway (this would normally be the DNS name of the installation)
gateway_tokenverifyversion  : indicates which version of "user" this gateway will be using for retreiving claims (TODO: refactor, so other services than user can be used) 

rabbitmq_host				: The IP adresse of the RabbitMQ server
rabbitmq_port				: The RabbitMQ port (normally 5672)
rabbitmq_username			: The username for RabbitMQ
rabbitmq_password			: The password for RabbitMQ


# Reference links

- [The Canine.Net project](https://canine.monosoft.dk/)
- [Monosoft development](https://dev.monosoft.dk/)
- [RabbitMQ homepage] (https://www.rabbitmq.com/)
- [Reactive Manifesto] (https://www.reactivemanifesto.org/)
- [Gitlab yaml](https://docs.gitlab.com/ee/ci/yaml/).

If you're new to Canine.Net development you'll want to check out the documentation and the tutorial, but if you're
already a seasoned developer with solid .NET, "clean code" and "clean architecture" experience considering building your own .NET
application for Canine.Net, this should all look very familiar.

## What's contained in this project

The repository folder structure (example/explanation):
```
    - docs										//the techical documentation is placed here, if there are any
    - src										//the source code is placed within this folder
	    - core
		    - application
				- [nameOfResource]				//
				    - commands
					    - get
						    command.cs
							response.cs
					    - getmany
						    command.cs
							request.cs
							response.cs
					    - insert
						    command.cs
							request.cs
			- domain
		- infrastructure
		    - external							//if the service is dependent on external ressources, these must be placed within this subfolder
				- [nameOfExternalService1]
				- [nameOfExternalService2]
			- Persistence.[nameOfTechnology1]
			- Persistence.[nameOfTechnology2]
		- presentation
			- [nameOfTechnology]				//This is where the application commands are registered for Canine/RabbitMQ.
    - test										//Unittest and integrations test are placed here, seperated from the business logic in src
    README.md 									//this document
	.gitlab-ci.yml 								//the gitlab yaml file for CI/CD pipeline
	.gitignore									//the git ignore file for this repository
	LICENSE										//the license for this repository
```


