FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster

COPY Src/Presentation/Gateway.Web/bin/Release/netcoreapp3.1/publish/ /App

WORKDIR /App

EXPOSE 80

ENTRYPOINT ["dotnet", "Gateway.Web.dll"]