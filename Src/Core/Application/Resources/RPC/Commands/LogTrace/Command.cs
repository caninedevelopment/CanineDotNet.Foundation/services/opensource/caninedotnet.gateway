namespace Application.Resources.RPC.Commands.LogTrace
{
    using Monosoft.Common.Command.Interfaces;

    public class Command : IProcedure<Request>
    {
        public Command()
        {
        }

        public void Execute(Request input)
        {
        }
    }
}