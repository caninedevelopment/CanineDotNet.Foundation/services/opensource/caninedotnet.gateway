﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Canine.Net.Infrastructure.Console;
using Canine.Net.Infrastructure.RabbitMQ.DTO;
using Canine.Net.Infrastructure.RabbitMQ.Message;
using Gateway.Web.Helpers;
using Gateway.Web.Hubs;
using Gateway.Web.Models;
using Gateway.Web.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.Writers;
using Monosoft.Common;

namespace Gateway.Web.Controllers
{
    public class RESTController : Controller
    {
        //[Produces("text/html")]
        [Route("api")]
        public string Help()
        {
            var returnMessage = RPCCall(this.HttpContext, "servicediscovery.v1.services.getall", "", false, null, true).Result;
            var serviceDescriptions = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptors>(returnMessage.Data);
            if (returnMessage.Success)
            {
                var document = new OpenApiDocument();
                document.Info = new OpenApiInfo();
                document.Info.Version = "all";
                document.Info.Title = "Canine API";
                document.Info.Description = "Gateway for Canine";
                document.Info.TermsOfService = new Uri("https://canine.monosoft.com/terms");
                document.Info.Contact = new OpenApiContact
                {
                    Name = "Monosoft ApS",
                    Email = string.Empty,
                    Url = new Uri("https://www.monosoft.dk/"),
                };



                document.Paths = new OpenApiPaths();
                document.Tags = new List<OpenApiTag>();

                foreach (var serviceDescription in serviceDescriptions.Services.OrderBy(p => p.Name).ThenBy(p => p.Version.ToString()))
                {
                    var openapiPath = new OpenApiPathItem();
                    var url = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/{serviceDescription.Name}/scarfoldTS";
                    var operationId = "scarfoldTS";
                    document.Paths.Add($"{serviceDescription.Name}/" + operationId, openapiPath);
                    OpenApiOperation openapiOperation = new OpenApiOperation
                    {
                        OperationId = operationId,
                        Description = "Scarfold TypeScript for this service" + $"<br/><a href='{url}'>{url}</a>",
                        Responses = new OpenApiResponses { },
                        RequestBody = null
                    };
                    openapiOperation.Responses.Add("200", new OpenApiResponse
                    {
                        Description = "ok",
                        Content = null
                    });
                    openapiOperation.Tags = new List<OpenApiTag>();
                    openapiOperation.Tags.Add(new OpenApiTag() { Name = serviceDescription.Name });//Group...
                    openapiPath.AddOperation(Microsoft.OpenApi.Models.OperationType.Get, openapiOperation);
                    foreach (var resourceDescription in serviceDescription.ResourceDescriptions)
                    {

                        foreach (var methodDescription in resourceDescription.OperationDescriptions)
                        {
                            openapiPath = new OpenApiPathItem();
                            operationId = $"v{serviceDescription.Version.Major}/{resourceDescription.Name}/{methodDescription.Operation}";
                            url = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/{serviceDescription.Name}/v{serviceDescription.Version.Major}/{resourceDescription.Name}/{methodDescription.Operation}";
                            openapiPath.Summary = serviceDescription.Name;

                            string claimstxt = "<br/>Required claims: ";
                            if (methodDescription.RequiredClaims != null && methodDescription.RequiredClaims.Any())
                            {
                                claimstxt += string.Join(", ", methodDescription.RequiredClaims.Select(p => $"<b>{p}</b>"));
                            }

                            if (methodDescription.Type == Canine.Net.Infrastructure.Console.OperationType.Event)
                            {
                                document.Paths.Add($"EVENTS({serviceDescription.Name}.v{serviceDescription.Version.Major}): " + methodDescription.Operation, openapiPath);
                                openapiOperation =
                                            new OpenApiOperation
                                            {
                                                OperationId = operationId,
                                                Description = methodDescription.Description,
                                                Responses = new OpenApiResponses { },
                                            };
                                openapiOperation.Responses.Add("200", new OpenApiResponse
                                {
                                    Description = $"Listen to signalR on hub: 'gateway' on '{methodDescription.Operation}'",
                                    Content = methodDescription.DataOutputExample == null ? null : new Dictionary<string, OpenApiMediaType>
                                    {
                                        ["application/json"] = new OpenApiMediaType
                                        {
                                            Schema = Api2OpenApi.Convert(methodDescription.DataOutputExample)
                                        },
                                    },
                                });

                                openapiOperation.Tags = new List<OpenApiTag>();
                                openapiOperation.Tags.Add(new OpenApiTag() { Name = serviceDescription.Name });//Group...
                            }
                            else
                            {
                                document.Paths.Add($"{serviceDescription.Name}/" + operationId, openapiPath);
                                openapiOperation =
                                            new OpenApiOperation
                                            {
                                                OperationId = operationId,
                                                Description = methodDescription.Description + claimstxt + $"<br/><a href='{url}'>{url}</a>",
                                                Responses = new OpenApiResponses { },
                                            };

                                if (methodDescription.Type == Canine.Net.Infrastructure.Console.OperationType.Get)
                                {
                                    openapiOperation.Parameters = new List<OpenApiParameter> {
                                            new OpenApiParameter() { In = ParameterLocation.Query,
                                //TODO:     Query2Json.ConvertTo()
                                             Name="param1_TODO:",
                                            Schema=Api2OpenApi.Convert(methodDescription.DataInputExample)
                                            }
                                        };
                                }
                                else
                                {
                                    openapiOperation.RequestBody = methodDescription.DataInputExample != null && methodDescription.DataInputExample.Properties.Any() ? new OpenApiRequestBody
                                    {
                                        Content = new Dictionary<string, OpenApiMediaType>
                                        {
                                            ["application/json"] = new OpenApiMediaType
                                            {
                                                Schema = Api2OpenApi.Convert(methodDescription.DataInputExample)
                                            },
                                        },
                                    }
                                        : null;

                                }


                                if (methodDescription.DataOutputExample != null)
                                {
                                    openapiOperation.Responses.Add("200", new OpenApiResponse
                                    {
                                        Description = "ok",
                                        Content = methodDescription.DataOutputExample == null ? null : new Dictionary<string, OpenApiMediaType>
                                        {
                                            ["application/json"] = new OpenApiMediaType
                                            {
                                                Schema = Api2OpenApi.Convert(methodDescription.DataOutputExample)
                                            },
                                        },
                                    });
                                }
                                else
                                {
                                    openapiOperation.Responses.Add("204", new OpenApiResponse { Description = "no content" });
                                }

                                if (methodDescription.RequiredClaims != null && methodDescription.RequiredClaims.Any())
                                {
                                    openapiOperation.Responses.Add("401", new OpenApiResponse { Description = "unauthorized" });
                                    openapiOperation.Responses.Add("403", new OpenApiResponse { Description = "forbidden" });
                                }
                                openapiOperation.Responses.Add("408", new OpenApiResponse { Description = "request timeout" });
                                openapiOperation.Responses.Add("500", new OpenApiResponse { Description = "internal server error" });

                                openapiOperation.Security = new List<OpenApiSecurityRequirement>();
                                openapiOperation.Tags = new List<OpenApiTag>();
                                openapiOperation.Tags.Add(new OpenApiTag() { Name = serviceDescription.Name });//Group...

                                if (methodDescription.RequiredClaims != null && methodDescription.RequiredClaims.Any())
                                {
                                    OpenApiSecurityRequirement requirement = new OpenApiSecurityRequirement();
                                    requirement.Add(new OpenApiSecurityScheme()
                                    {//TODO: TEST
                                     //BearerFormat = "Bearer",
                                     //Scheme = "Bearer",
                                        Description = "TokenId from login",
                                        Name = "TokenId",
                                        In = ParameterLocation.Header,
                                        Type = SecuritySchemeType.ApiKey,
                                    }, methodDescription.RequiredClaims);

                                    openapiOperation.Security.Add(requirement);
                                }
                            }
                            switch (methodDescription.Type)
                            {
                                case Canine.Net.Infrastructure.Console.OperationType.Get:
                                    openapiPath.AddOperation(Microsoft.OpenApi.Models.OperationType.Get, openapiOperation);
                                    break;
                                case Canine.Net.Infrastructure.Console.OperationType.Update:
                                    openapiPath.AddOperation(Microsoft.OpenApi.Models.OperationType.Patch, openapiOperation);
                                    break;
                                case Canine.Net.Infrastructure.Console.OperationType.Insert:
                                    openapiPath.AddOperation(Microsoft.OpenApi.Models.OperationType.Post, openapiOperation);
                                    break;
                                case Canine.Net.Infrastructure.Console.OperationType.Delete:
                                    openapiPath.AddOperation(Microsoft.OpenApi.Models.OperationType.Delete, openapiOperation);
                                    break;
                                case Canine.Net.Infrastructure.Console.OperationType.Event:
                                    openapiPath.AddOperation(Microsoft.OpenApi.Models.OperationType.Options, openapiOperation);
                                    break;
                                default:
                                    openapiPath.AddOperation(Microsoft.OpenApi.Models.OperationType.Patch, openapiOperation);
                                    break;
                            }
                        }
                    }
                }
                using (var outputString = new StringWriter())
                {
                    var writer = new OpenApiJsonWriter(outputString);
                    document.SerializeAsV3(writer);
                    return outputString.ToString();
                }
            }
            else
            {
                return "";
            }
        }

        public class ServicesInformation
        {
            public string Name { get; set; }
            public ServiceDescriptor Description { get; set; }
        }

        [Route("internal/directlink/{folder}/{file}")]
        public async Task<string> directlink(string folder, string file)
        {
            return await System.IO.File.ReadAllTextAsync(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory() + @"\directlink\" + folder, file));
        }

        [Route("api/log")]
        public string Log()
        {
            return System.IO.File.ReadAllText(DateTime.Today.ToString("yyyy_MM_dd") + ".log");
        }

        [HttpGet("api/directlink/{id}")]
        public async Task<string> IndexGet(Guid id)
        {
            return await GatewayHub.GetDirectLinkData(id);
        }

        [HttpGet("{service?}/{version?}/{resource?}/{operation?}")]
        public async Task<string> IndexGetData(string service, string version, string resource, string operation)
        {
            try
            {
                var route = $"{service}.{version}.{resource}.{operation}";

                string json = "{}";
                if (string.IsNullOrEmpty(HttpContext.Request.QueryString.Value) == false)
                {
                    json = Query2Json.ConvertTo(HttpContext.Request.QueryString.Value.Substring(1)/*remove ?*/);
                }
                var res = await RPCCall(HttpContext, route, json, false);
                HttpContext.Response.StatusCode = res.HTTPStatusCode;
                if (res.HTTPStatusCode == 500)
                {
                    return res.Data;//benyt .data her, da interne serverfejl har trace i .data feltet
                }
                else
                if (res.HTTPStatusCode < 200 || res.HTTPStatusCode > 299)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(res.Message);
                }
                else
                {
                    return res.Data; // Newtonsoft.Json.JsonConvert.DeserializeObject(res.data) as string;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPost("{service?}/{version?}/{resource?}/{operation?}")]
        [HttpPut("{service?}/{version?}/{resource?}/{operation?}")]
        [HttpDelete("{service?}/{version?}/{resource?}/{operation?}")]
        public async Task<string> IndexBodyData(string service, string version, string resource, string operation)
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var json = reader.ReadToEnd();
                    var route = $"{service}.{version}.{resource}.{operation}";
                    var res = await RPCCall(HttpContext, route, json, false);
                    HttpContext.Response.StatusCode = res.HTTPStatusCode;
                    if (res.HTTPStatusCode == 500)
                    {
                        return res.Data;//benyt .data her, da interne serverfejl har trace i .data feltet
                    }
                    else
                    if (res.HTTPStatusCode < 200 || res.HTTPStatusCode > 299)
                    {
                        return Newtonsoft.Json.JsonConvert.SerializeObject(res.Message);
                    }
                    else
                    {
                        return res.Data; // Newtonsoft.Json.JsonConvert.DeserializeObject(res.data) as string;
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet("api/{service?}/{version?}/{operation?}/{resource?}/{diagnostics?}")]
        public async Task<string> IndexGet(string service, string version, string resource, string operation, bool diagnostics = false)
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var json = reader.ReadToEnd();

                    var route = $"{service}.{version}.{operation}";
                    return Newtonsoft.Json.JsonConvert.SerializeObject(await RPCCall(HttpContext, route, json, diagnostics));
                }
            }
            catch (Exception ex)
            {
                //RPCController.LogTrace($".RPCCall : " + ex.Message);
                return ex.Message;
            }
        }

        [HttpPut("api/{service?}/{version?}/{operation?}/{resource?}/{diagnostics?}")]
        [HttpPost("api/{service?}/{version?}/{operation?}/{resource?}/{diagnostics?}")]
        [HttpDelete("api/{service?}/{version?}/{operation?}/{resource?}/{diagnostics?}")]
        public async Task<string> IndexPost(string service, string version, string resource, string operation, bool diagnostics = false)
        {
            using (var reader = new StreamReader(Request.Body))
            {
                var json = reader.ReadToEnd();

                var route = $"{service}.{version}.{operation}";
                return Newtonsoft.Json.JsonConvert.SerializeObject(await RPCCall(HttpContext, route, json, diagnostics));
            }
        }

        private static ServiceDescriptors services = null;
        public static async Task<GatewayReturnMessage> RPCCall(HttpContext context, string route, string requestJson, bool includeDiagnostics, Guid? token = null, bool skip404check = false)
        {
            route = route.ToLowerInvariant();
            string messageId = "";
            var routeparts = route.Split('.');

            if (routeparts[0] == "servicediscovery")
                skip404check = true;

            if (context.Request.Headers.ContainsKey("MessageId"))
            {
                messageId = context.Request.Headers["MessageId"];
            }
            if (routeparts.Length >= 2 && routeparts[1] == "scarfoldTS")
            {
                skip404check = true;
            }


            int statuscode = StatusCodes.Status200OK;
            OperationDescription method = null;
            if (skip404check == false)
            {
                if (services == null)
                {
                    var returnMessage = RPCCall(context, "servicediscovery.v1.services.getall", "", false, token, true).Result;
                    services = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptors>(returnMessage.Data);
                }
                if (services != null)
                {

                    method = (from ser in services.Services
                              from res in ser.ResourceDescriptions
                              from op in res.OperationDescriptions
                              where route == $"{ser.Name}.v{ser.Version.Major}.{res.Name}.{op.Operation}".ToLowerInvariant()
                              select op
                               ).FirstOrDefault();
                }
                if (method == null)
                {//REFRESH
                    var returnMessage = RPCCall(context, "servicediscovery.v1.services.getall", "", false, token, true).Result;
                    services = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptors>(returnMessage.Data);
                    method = (from ser in services.Services
                              from res in ser.ResourceDescriptions
                              from op in res.OperationDescriptions
                              where route == $"{ser.Name}.v{ser.Version.Major}.{res.Name}.{op.Operation}".ToLowerInvariant()
                              select op

                               ).FirstOrDefault();
                }

                if (method != null)
                {

                    if (method.DataOutputExample != null) //method.Type == OperationDescription.OperationType.Get)
                    {
                        statuscode = StatusCodes.Status200OK;
                    }
                    else
                    {
                        statuscode = StatusCodes.Status204NoContent;
                    }
                }
            }

            if (skip404check || method != null)
            {
                route = route.Trim('.');
                string ip = context.Connection.RemoteIpAddress.ToString();

                Guid tokenid = token.HasValue ? token.Value : Guid.Empty;

                if (context.Request.Headers.ContainsKey("TokenId"))
                {
                    tokenid = Guid.Parse(context.Request.Headers["TokenId"]);
                }
                if (context.Request.Headers.ContainsKey("tokenid"))
                {
                    tokenid = Guid.Parse(context.Request.Headers["tokenid"]);
                }
                if (context.Request.Cookies.Keys.Contains("TokenId"))
                {
                    tokenid = Guid.Parse(context.Request.Cookies["TokenId"]);
                }
                if (context.Request.Cookies.Keys.Contains("tokenid"))
                {
                    tokenid = Guid.Parse(context.Request.Cookies["tokenid"]);
                }

                Guid organisationId = Guid.Empty;
                if (context.Request.Headers.ContainsKey("OrganisationId"))
                {
                    organisationId = Guid.Parse(context.Request.Headers["OrganisationId"]);
                }
                string clientid = string.Empty;


                TokenInfo tokendata = new TokenInfo() { Claims = new List<string>(), TokenId = tokenid, UserId = Guid.Empty, ValidUntil = DateTime.Now };
                try
                {
                    if (tokenid != Guid.Empty)
                    {
                        tokendata = await MemoryCache.FindToken(tokenid);
                    }

                }
                catch
                {
                    return new GatewayReturnMessage()
                    {
                        Success = false,
                        HTTPStatusCode = StatusCodes.Status401Unauthorized,
                        Message = new LocalizedString(new Guid("C355EC1A-FA7C-4DEF-9A24-A4D1BEF7CFE1"), "Unauthorized"),
                        Diagnostics = "",
                        Route = route,
                        ResponseToMessageId = messageId,
                        Data = "{}",
                        DataType = "json"
                    };
                }


                var res = await RPCController.DoRPCCall(route, new RabbitMqHeader()
                {
                    ClientId = clientid,
                    TokenInfo = tokendata,
                    MessageIssueDate = DateTime.Now,
                    Ip = ip,
                    IsDirectLink = false,
                    IncludeDiagnostics = includeDiagnostics,
                    MessageId = messageId
                }, requestJson);

                if (res.Success == false && res.Message != null && res.Message.Text == "Missing credentials")
                {
                    statuscode = StatusCodes.Status403Forbidden;
                }
                else
                if (res.Success == false && res.Message != null && res.Message.Text.StartsWith("Request timed out"))
                {
                    statuscode = StatusCodes.Status408RequestTimeout;
                }
                else
                if (res.Success == false && res.Message != null && res.Message.Text != "Error")
                {
                    statuscode = Error2HttpCode.GetHttpCode(res.Message);
                }
                else if (res.Success == false)
                {
                    statuscode = StatusCodes.Status500InternalServerError;
                }

                res.HTTPStatusCode = statuscode;

                return res;
            }
            else
            {
                return new GatewayReturnMessage()
                {
                    Success = false,
                    HTTPStatusCode = StatusCodes.Status501NotImplemented,
                    Message = new LocalizedString(new Guid("4F94AA3E-9102-4FC0-9E2B-06CFB0509979"), "Not implemented"),
                    Route = route,
                    ResponseToMessageId = messageId,
                    Data = "{}",
                    DataType = "json"
                };

            }
        }
    }
}
