﻿namespace Gateway.Web.Controllers
{
    using System;
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Gateway.Web.Models.DTOs;
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Gateway.Web.Persistence;
    using Canine.Net.Infrastructure.RabbitMQ.Message;
    using Gateway.Web.Models;
    using Canine.Net.Infrastructure.RabbitMQ.Request;

    [Route("api/[controller]")]
    [ApiController]
    public class RPCController : ControllerBase
    {
        [HttpPost]
        [RequestSizeLimit(524288000)]
        public async Task<string> Post([FromBody] ApiMessage message)
        {
            string ip = HttpContext.Connection.RemoteIpAddress.ToString();
            string clientid = string.Empty;

            TokenInfo tokendata = new TokenInfo() { Claims = new List<string>(), TokenId = message.Token, UserId = Guid.Empty, ValidUntil = DateTime.Now };
            if (message.Token != Guid.Empty)
            {
                tokendata = await MemoryCache.FindToken(message.Token);
            }

            var res = await RPCController.DoRPCCall(message.Route, new RabbitMqHeader() { ClientId = clientid, Ip = ip, IsDirectLink = false, MessageId = message.MessageId, MessageIssueDate = DateTime.Now, TokenInfo = tokendata }, message.Json);
            return Newtonsoft.Json.JsonConvert.SerializeObject(res);
        }

        private static readonly object filepadlock = new object();
        public static void LogTrace(string data)
        {
            lock (filepadlock)
            {
                System.IO.File.AppendAllText(DateTime.Today.ToString("yyyy_MM_dd") + ".log", $"{DateTime.Now} {data}");
            }
        }
        public static async Task<GatewayReturnMessage> DoRPCCall(string route, RabbitMqHeader header, string messageAsJson)
        {
            try
            {
                using (var client = RequestClient.RPCInstance)
                {
                    var response = await client.Rpc(route.ToLower(), header, messageAsJson, Program.config.InternalServername);

                    if (response == null)
                    {
                        LogTrace($"RPC -ERROR: response IS NULL, on route:{route} clientid:{header.ClientId}  messageid:{header.MessageId}");
                        return null;
                    }

                    return new GatewayReturnMessage(response);
                }
            }
            catch (Exception ex)
            {
                LogTrace($"Critical error: " + ex.Message);
                return null;
            }
        }
    }
}
