﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Gateway.Web.Controllers
{
    public class FILEController : Controller
    {
        [HttpGet("api/file/{service?}/{version?}/{operation?}/{json}/{path}/{token}")]
        public async Task<FileResult> Get(string service, string version, string operation, string json, string path, string token)
        {
            var route = $"{service}.{version}.{operation}";
            var res = await Controllers.RESTController.RPCCall(HttpContext, route, json, false, string.IsNullOrEmpty(token) ? null : (Guid?)Guid.Parse(token));
            var resjson = res.Data;

            string base64file = JObject.Parse(resjson).SelectToken(path).ToString();

            return File(Convert.FromBase64String(base64file), System.Net.Mime.MediaTypeNames.Application.Octet, route + "." + path);
        }

        [HttpPut("api/file/{service?}/{version?}/{operation?}/{path?}")]
        [HttpPost("api/file/{service?}/{version?}/{operation?}/{path?}")]
        public async Task<object> Get(string service, string version, string operation, string path)
        {
            using (var reader = new StreamReader(Request.Body))
            {
                try
                {
                    var json = reader.ReadToEnd();
                    var route = $"{service}.{version}.{operation}";

                    var res = await RESTController.RPCCall(HttpContext, route, json, false);
                    if (res.Success == true)
                    {
                        var resjson = res.Data;
                        string base64file = JObject.Parse(resjson).SelectToken(path).ToString();
                        return File(Convert.FromBase64String(base64file), System.Net.Mime.MediaTypeNames.Application.Octet, route + "." + path);
                    }
                    else throw new Exception(res.Data);

                }
                catch (Exception ex)
                {
                    HttpContext.Response.StatusCode = 500;
                    return ex.Message;
                }
            }
        }
    }
}