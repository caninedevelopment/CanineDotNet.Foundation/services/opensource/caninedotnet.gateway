﻿using System;
using System.Collections.Generic;
using Canine.Net.Infrastructure.RabbitMQ.DTO;
using Canine.Net.Infrastructure.RabbitMQ.Message;
using Canine.Net.Infrastructure.RabbitMQ.Request;
using Gateway.Web.Models.DTOs;
using Gateway.Web.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace Gateway.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FAFController : ControllerBase
    {
        [HttpPost]
        [RequestSizeLimit(524288000)]
        public async void Post([FromBody] ApiMessage message)
        {
            string ip = HttpContext.Connection.RemoteIpAddress.ToString();
            string clientid = string.Empty;
            TokenInfo tokendata = new TokenInfo() { Claims = new List<string>(), TokenId = message.Token, UserId = Guid.Empty, ValidUntil = DateTime.Now };
            if (message.Token != Guid.Empty)
            {
                tokendata = await MemoryCache.FindToken(message.Token);
            }
            RequestClient.FAFInstance.FAF(message.Route, new RabbitMqHeader()
            {
                ClientId = clientid,
                Ip = ip,
                IsDirectLink = false,
                MessageId = message.MessageId,
                MessageIssueDate = DateTime.Now,
                TokenInfo = tokendata
            }, message.Json, Program.config.InternalServername);
        }
    }
}