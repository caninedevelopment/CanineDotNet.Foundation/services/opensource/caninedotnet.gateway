﻿namespace Gateway.Web.Helpers
{
    using Microsoft.AspNetCore.Http;
    using Monosoft.Common;

    public static class Error2HttpCode
    {
        public static int GetHttpCode(LocalizedString errorMsg)
        {
            // TODO find better way to check http code.
            if (errorMsg != null)
            {
                // Null or default
                if (errorMsg.Key.ToString() == "13effc5c-709d-4102-a854-c772e16ca294")
                {
                    return StatusCodes.Status400BadRequest;
                }
                // Element does not exixst
                if (errorMsg.Key.ToString() == "4525ab8c-a95a-4ab6-9677-3d4b1532b42d")
                {
                    return StatusCodes.Status410Gone;
                }
                // Element already exist
                if (errorMsg.Key.ToString() == "ae3f4b65-fa8c-4b4e-a612-b6328bdc8099")
                {
                    return StatusCodes.Status409Conflict;
                }
                // Validation
                if (errorMsg.Key.ToString() == "f19f64c3-bd30-48cd-93d4-706d139a3740")
                {
                    return StatusCodes.Status400BadRequest;
                }
            }
            return StatusCodes.Status500InternalServerError;
        }
    }
}
