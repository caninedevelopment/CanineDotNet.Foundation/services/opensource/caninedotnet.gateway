﻿namespace Gateway.Web.Helpers
{
    using Canine.Net.Infrastructure.RabbitMQ.Message;
    using Microsoft.OpenApi;
    using Microsoft.OpenApi.Any;
    using Microsoft.OpenApi.Models;
    using Microsoft.OpenApi.Writers;
    using System.Linq;
    using static Canine.Net.Infrastructure.RabbitMQ.Message.ApiSchema;

    public static class Api2OpenApi
    {
        public static OpenApiSchema Convert(ApiSchema scheme)
        {
            // TODO færdiggøre?
            OpenApiSchema openScheme = new OpenApiSchema();
            if (scheme != null)
            {
                openScheme.Description = scheme.Description;
                openScheme.Title = scheme.Name;
                openScheme.Example = new OpenApiString(GetExample(scheme.DataType));
                openScheme.Type = scheme.DataType.ToString();
                openScheme.Nullable = scheme.IsNullable;
                //openScheme.Enum = scheme.ValidValues != null ? scheme.ValidValues.Select(x => new OpenApiString(x)).ToList() : null;
                openScheme.Default = GetDefault(scheme.DataType);
                openScheme.Properties = scheme.Properties.Where(x => string.IsNullOrEmpty(x.Name) == false).ToDictionary(key => key.Name, value => Convert(value));
            }
            return openScheme;
        }

        static IOpenApiAny GetDefault(DataTypeEnum type)
        {
            switch (type)
            {
                case DataTypeEnum.Object:
                    return new OpenApiString("Default");
                case DataTypeEnum.Array:
                    return new OpenApiString("[]");
                case DataTypeEnum.String:
                    return new OpenApiString("");
                case DataTypeEnum.Number:
                case DataTypeEnum.BigInt:
                case DataTypeEnum.UnsignedBigInt:
                case DataTypeEnum.Decimal:
                case DataTypeEnum.UnsignedNumber:
                    return new OpenApiInteger(0);
                case DataTypeEnum.Boolean:
                    return new OpenApiBoolean(false);
                case DataTypeEnum.Enum:
                    return new OpenApiInteger(0);
                case DataTypeEnum.Guid:
                    return new OpenApiString("00000000-0000-0000-0000-000000000000");
                default:
                    return new OpenApiString("Default");
            }
        }

        static string GetExample(DataTypeEnum type)
        {
            switch (type)
            {
                case DataTypeEnum.Object:
                    return "{}";
                case DataTypeEnum.Array:
                    return "[]";
                case DataTypeEnum.String:
                    return "Hello world!";
                case DataTypeEnum.Number:
                case DataTypeEnum.BigInt:
                case DataTypeEnum.UnsignedBigInt:
                case DataTypeEnum.UnsignedNumber:
                    return "9";
                case DataTypeEnum.Decimal:
                    return "5.6";
                case DataTypeEnum.Boolean:
                    return "True";
                case DataTypeEnum.Enum:
                    return "None";
                case DataTypeEnum.Guid:
                    return "00000000-0000-0000-0000-000000000000";
                default:
                    return "Default";
            }
        }
    }
}
