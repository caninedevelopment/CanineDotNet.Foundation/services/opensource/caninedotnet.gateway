﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Web.Helpers
{
    public class Query2Json
    {
        public static string ConvertTo(string querystring)
        {
            var obj = new JObject();
            //find hver af vores parametre i vores query string. Bemærk jeg forventer den allerede er splitted på '?'
            var pairs = querystring.Split('&');
            //pairs er key og value pairs. dvs. user.name (key) = ruben (value)
            foreach (var pair in pairs)
            {
                var keyvalue = pair.Split('=');
                var key = keyvalue[0];//(user.name)
                var value = keyvalue[1];//(ruben)
                var pathsteps = key.Split('.');// split vores key op i steps for hver . så vi kan navigere ind i vores objekt. først user så name.
                obj = (JObject)populateObject(obj, pathsteps.ToList(), value); //let the magic begin. Jtoken == object i Newtonsofts verden, kan kan være hvad som helst. Jobject == {} Jarray == [] JValue == ""
            }
            return obj.ToString();
        }

        public static List<KeyValuePair<string, string>> ConvertFrom(string json)
        {
            List<KeyValuePair<string, string>> res = new List<KeyValuePair<string, string>>();
            var jsonobj = JObject.Parse(json);
            foreach (var child in jsonobj.Children().Cast<JProperty>())
            {
                res.AddRange(Convert(child, new List<string>() { child.Name }));
            }
            return res;
        }

        private static List<KeyValuePair<string, string>> Convert(JProperty obj, List<string> prefix)
        {
            var res = new List<KeyValuePair<string, string>>();
            foreach (var child in obj.Children().Cast<JProperty>())
            {
                var fullname = new List<string>();
                fullname.AddRange(prefix);
                fullname.Add(child.Name);

                if (child.Type == JTokenType.Array)
                {
                    res.Add(new KeyValuePair<string, string>(string.Join('.', fullname), child.Type.ToString()));
                }
                else
                {
                    res.Add(new KeyValuePair<string, string>(string.Join('.', fullname) + "[]", child.Type.ToString()));
                }
                res.AddRange(Convert(child, fullname));
            }

            return res;
        }


        public static JToken populateObject(JToken obj, List<string> path, string value)
        {
            var firstStep = path[0]; //find navnet på det object vi skal tilføje
            var isarray = firstStep.Contains('[') && firstStep.Contains(']'); //hvis der er [###] i navnet så er det et array
            firstStep = isarray ? firstStep.Split('[')[0] : firstStep;// fjern fra og med '[' hvis det er et array. så orgs[1] bliver til orgs BEMÆRK jeg går stærkt ud fra tingene kommer i rigtig rækkefølge eller det bliver totalt forkert!
            var isLastStep = path.Count == 1;// er der ikke flere pathsteps, altså vi er nået til niveau 'name' hvor der skal assignes in JValue, altså en string eller lign.

            if (obj == null)// måske noget lorte kode, men det virker.. dont ask me..
                obj = new JObject();
            var token = obj.SelectToken(firstStep, false); //find det niveau vi prøver at opdatere, så f.eks. user, (den eksisterer hvis vi har tidligere givet user en property)

            if (isLastStep)
            {
                //Hvis det ikke er et array tilføj vores simpel værdi her, vi er på yderste niveau.
                if (!isarray)
                    ((JObject)obj).Add(firstStep, value);
                else
                {
                    //Hvis vi er på last step og det er blot et array med simple værdier:
                    //hvis token er null er det fordi array er ikke eksisterende endnu, så vi skal create det. Hvis det eksisterer skal vi add til det. (BEMÆRK den tager ikke højde for indexet angivet i query string!! users[INDEX])
                    if (token is null)
                    {
                        var arr = new JArray(value);
                        ((JObject)obj).Add(firstStep, value);
                    }
                    else
                        ((JArray)token).Add(value);
                }
            }
            else
            {
                //hvis vi ikke er på yderste niveau
                if (token is null)
                {
                    //hvis objektet ikke eksisterer endnu, altså det er første gang vi prøver at tilføje en property på f.eks. user
                    JToken newobject;
                    //sørger for vi får users: []
                    if (isarray)
                        newobject = new JArray(populateObject(null, path.Skip(1).ToList(), value));
                    else
                        //sørger for vi får user: {}
                        newobject = populateObject(null, path.Skip(1).ToList(), value);
                    //tilføj til vores objekt.
                    ((JObject)obj).Add(firstStep, newobject);

                }
                else
                {
                    //sørger for vi får users: []
                    if (isarray)
                    {
                        var arr = new JArray(value);
                        ((JObject)obj[firstStep]).Add(firstStep, populateObject(null, path.Skip(1).ToList(), value));
                    }
                    else
                        //sørger for vi får user: {}
                        //merge fordi objektet allerede eksisterer, så user.name og user.lastname bliver til user objekt med begge værdier på.
                        ((JObject)obj[firstStep]).Merge(populateObject(null, path.Skip(1).ToList(), value));
                }
            }
            return obj;
        }
    }
}