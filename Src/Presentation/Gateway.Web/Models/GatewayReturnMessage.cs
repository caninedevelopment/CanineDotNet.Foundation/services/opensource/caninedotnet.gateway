﻿using Canine.Net.Infrastructure.RabbitMQ.Message;
using Monosoft.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Web.Models
{
    public class GatewayReturnMessage
    {
        public GatewayReturnMessage()
        {
        }
        public GatewayReturnMessage(ReturnMessage data)
        {
            this.Diagnostics = data.Diagnostics;
            this.ResponseToMessageId = data.ResponseToMessageId;
            this.Message = data.Message;
            this.Success = data.Success;
            this.Data = data.Data == null ? "" : System.Text.Encoding.UTF8.GetString(data.Data);
            this.DataType = "json";
            this.HTTPStatusCode = 200;
        }

        public string ResponseToMessageId { get; set; }
        public LocalizedString Message { get; set; }
        public bool Success { get; set; }
        public string Diagnostics { get; set; }
        public string Route { get; set; }
        public string Data { get; set; }
        public string DataType { get; set; }
        public int HTTPStatusCode { get; set; }

    }
}
