﻿using System;

namespace Gateway.Web.Models
{
    public class DirectLinkInfo
    {
        public DateTime Timestamp { get; set; }
        public string Json { get; set; }
    }
}
