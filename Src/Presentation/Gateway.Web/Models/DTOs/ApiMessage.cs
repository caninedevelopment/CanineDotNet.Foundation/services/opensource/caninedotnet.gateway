﻿namespace Gateway.Web.Models.DTOs
{
    using System;

    public class ApiMessage
    {
        public string Route { get; set; }

        public string MessageId { get; set; }

        public string Json { get; set; }

        public Guid Token { get; set; }

        //        public Tracing.Level tracing { get; set; }
    }
}
