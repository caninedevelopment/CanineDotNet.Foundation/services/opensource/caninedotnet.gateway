﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Web
{
    public class ExceptionHelper
    {
        public static string GetExceptionAsReportText(Exception ex)
        {
            var res = ex.StackTrace.ToString();
            var currentEx = ex;
            while (currentEx != null)
            {
                res += ex.Message + "\r\n";
                currentEx = currentEx.InnerException;
            }

            return res;
        }
    }
}
