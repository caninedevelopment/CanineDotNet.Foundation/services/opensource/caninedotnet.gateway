using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Gateway.Web.Hubs;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using Gateway;
using System.Collections.Generic;
using Gateway.Web.Models;
using Canine.Net.Infrastructure.RabbitMQ.Message;
using Canine.Net.Infrastructure.RabbitMQ.Event;
using Gateway.Web.Controllers;
using Gateway.Web.Persistence;
using Canine.Net.Infrastructure.RabbitMQ.Request;
using Canine.Net.Infrastructure.Config;

namespace Gateway.Web
{
    public static class Program
    {
        public static ServerConfiguration config = new ServerConfiguration()
        {
            CORS = System.Environment.GetEnvironmentVariable("gateway_cors"),
            InternalServername = System.Environment.GetEnvironmentVariable("gateway_internalservername"),
            TokenVerifyVersion = System.Environment.GetEnvironmentVariable("gateway_tokenverifyversion")
        };
        public static void Main(string[] args)
        {

            List<MessageQueueConfiguration> configurations = new List<MessageQueueConfiguration>();
            configurations.Add(new EventConfiguration(
                "gateway",
                new List<string>() { "#" },
                GatewayHub.HandleMessage
                ));

            configurations.Add(new EventConfiguration(
                "gateway_invalidate_user",
                new List<string>() { "token.invalidate.user" },
                MemoryCache.HandleInvalidateUser
                ));
            configurations.Add(new EventConfiguration(
                "gateway_invalidate_token",
                new List<string>() { "token.invalidate.token" },
                MemoryCache.HandleInvalidateToken
                ));


            using (var server = new RequestServer(configurations, GetSettings()))
            {
                //server.connection.RecoverySucceeded += Connection_RecoverySucceeded;
                server.connection.ConnectionShutdown += Connection_ConnectionShutdown;
                //server.connection.ConnectionRecoveryError += Connection_ConnectionRecoveryError;
                server.connection.ConnectionBlocked += Connection_ConnectionBlocked;
                server.connection.CallbackException += Connection_CallbackException;
                CreateWebHostBuilder(args).Build().Run();
            }
        }

        private static RabbitMQSettings GetSettings()
        {
            var rabbitMQUsername = System.Environment.GetEnvironmentVariable("rabbitmq_username");
            var rabbitMQPassword = System.Environment.GetEnvironmentVariable("rabbitmq_password");
            var rabbitMQHost = System.Environment.GetEnvironmentVariable("rabbitmq_host");
            var rabbitMQPort = System.Environment.GetEnvironmentVariable("rabbitmq_port");

            return new RabbitMQSettings(rabbitMQUsername, rabbitMQPassword, rabbitMQHost, rabbitMQPort != null ? int.Parse(rabbitMQPort) : 5672);
        }

        private static void Connection_CallbackException(object sender, CallbackExceptionEventArgs e)
        {
            var details = Newtonsoft.Json.JsonConvert.SerializeObject(e.Detail);
            var exception = ExceptionHelper.GetExceptionAsReportText(e.Exception);
            RPCController.LogTrace($".EventClient_Connection_CallbackException.log : " + details + "\r\n" + exception);
        }

        private static void Connection_ConnectionBlocked(object sender, ConnectionBlockedEventArgs e)
        {
            RPCController.LogTrace($".EventClient_Connection_ConnectionBlocked.log : " + e.Reason);
        }

        private static void Connection_ConnectionRecoveryError(object sender, ConnectionRecoveryErrorEventArgs e)
        {
            var exception = ExceptionHelper.GetExceptionAsReportText(e.Exception);
            RPCController.LogTrace($".EventClient_Connection_ConnectionRecoveryError.log : " + exception);
        }

        private static void Connection_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            RPCController.LogTrace($".EventClient_Connection_ConnectionShutdown.log : " + e.ReplyText);
        }

        private static void Connection_RecoverySucceeded(object sender, EventArgs e)
        {
            RPCController.LogTrace($".EventClient_Connection_RecoverySucceeded.log : true");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
    }

}
