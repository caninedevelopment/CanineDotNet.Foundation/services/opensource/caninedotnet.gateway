﻿namespace Gateway.Web.Persistence
{
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Canine.Net.Infrastructure.RabbitMQ.Message;
    using Canine.Net.Infrastructure.RabbitMQ.Request;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Contains static lists for in-memory representation of user and token data in order to reduce load on the database
    /// </summary>
    public class MemoryCache
    {
        private static MemoryCache instance = null;

        public static MemoryCache Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MemoryCache();
                }
                return instance;
            }
        }

        private readonly List<TokenInfo> TokenCache = new List<TokenInfo>();

        /// <summary>
        /// Clear a specific token from the cache
        /// </summary>
        /// <param name="tokenId">the tokenid to remove from the cache</param>
        internal void InvalidateToken(TokenInfo token)
        {//TODO: mangler listninger
            foreach (var oldtoken in MemoryCache.Instance.TokenCache.Where(p => p.TokenId == token.TokenId).ToList())
            {
                oldtoken.ValidUntil = token.ValidUntil;
            }
        }

        /// <summary>
        /// Clear a specific token from the cache
        /// </summary>
        /// <param name="tokenId">the tokenid to remove from the cache</param>
        internal void InvalidateUsers(InvalidateUserData data)
        {
            foreach (var user in data.userIds)
            {
                foreach (var oldtoken in MemoryCache.Instance.TokenCache.Where(p => p.UserId == user).ToList())
                {
                    oldtoken.ValidUntil = data.validUntil;
                }
            }
        }
        public static void HandleInvalidateUser(string[] topicparts, ReturnMessage wrapper)
        {
            var json = System.Text.Encoding.UTF8.GetString(wrapper.Data);
            InvalidateUserData user = Newtonsoft.Json.JsonConvert.DeserializeObject<InvalidateUserData>(json);
            Instance.InvalidateUsers(user);
        }
        public static void HandleInvalidateToken(string[] topicparts, ReturnMessage wrapper)
        {
            var json = System.Text.Encoding.UTF8.GetString(wrapper.Data);
            TokenInfo token = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenInfo>(json);
            Instance.InvalidateToken(token);
        }


        /// <summary>
        /// Add token data to the memeory cache
        /// </summary>
        /// <param name="token">The token to add</param>
        internal static void AddToken(TokenInfo token)
        {
            MemoryCache.Instance.TokenCache.Add(token);
        }

        /// <summary>
        /// Find tokendata in the cache from a token - will automatically get from remote server (RPC) if needed
        /// </summary>
        /// <param name="token">The token to find</param>
        /// <param name="orgContext">The organisation context to find it in</param>
        /// <returns>The found token data</returns>
        public static async Task<TokenInfo> FindToken(Guid tokenid)
        {
            if (tokenid != Guid.Empty)
            {
                var res = MemoryCache.Instance.TokenCache.Where(p => p.TokenId == tokenid).FirstOrDefault();
                if (res == null)
                {
                    using (var client = RequestClient.RPCInstance)
                    {
                        string useVersion = string.IsNullOrEmpty(Program.config.TokenVerifyVersion) ? "v1" : Program.config.TokenVerifyVersion;

                        ReturnMessage result = client.Rpc("user." + useVersion + ".token.verify", new RabbitMqHeader()
                        {
                            ClientId = "N/A",
                            MessageId = "N/A",
                            MessageIssueDate = DateTime.Now,
                            IsDirectLink = false,
                            TokenInfo = new TokenInfo()
                            {
                                TokenId = tokenid,
                                ValidUntil = DateTime.Now.AddMinutes(1)
                            }
                        },
                        "",
                        "N/A").Result;

                        if (result.Success)
                        {
                            var innerres = System.Text.Encoding.UTF8.GetString(result.Data);
                            res = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenInfo>(innerres);
                            if (res != null)
                                MemoryCache.Instance.TokenCache.Add(res);
                        }
                        else
                        {
                            throw new Exception("Unable to verify token: " + Newtonsoft.Json.JsonConvert.SerializeObject(result.Message));
                        }
                    }
                }
                return res;
            }
            else
            {
                throw new Exception("TokenId should not be GUID.Empty");
            }
        }
    }
}
