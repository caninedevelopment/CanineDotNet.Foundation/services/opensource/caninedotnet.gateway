﻿using Canine.Net.Infrastructure.RabbitMQ.Message;
using Canine.Net.Infrastructure.RabbitMQ.Request;
using Gateway.Web.Controllers;
using Gateway.Web.Models;
using Gateway.Web.Persistence;
using Microsoft.AspNetCore.SignalR;
using Monosoft.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Web.Hubs
{
    public class GatewayHub : Hub
    {
        public async void WriteMessage(
            string route,
            string messageid,
            string json,
            Guid userContextToken
            )
        {
            string ip = this.Context.GetHttpContext().Connection.RemoteIpAddress.ToString();
            string clientid = this.Context.ConnectionId;
            var tokendata = await MemoryCache.FindToken(userContextToken);
            RequestClient.FAFInstance.FAF(route, new RabbitMqHeader() { ClientId = clientid, IsDirectLink = false/*?*/, MessageId = messageid, MessageIssueDate = DateTime.Now, TokenInfo = tokendata, Ip = ip }, json, Gateway.Web.Program.config.InternalServername);
        }

        public async Task<string> RPC(
            string route,
            string messageid,
            string json,
            Guid userContextToken
            )
        {
            string ip = this.Context.GetHttpContext().Connection.RemoteIpAddress.ToString();
            string clientid = this.Context.ConnectionId;
            var tokendata = await MemoryCache.FindToken(userContextToken);
            var result = RPCController.DoRPCCall(route, new RabbitMqHeader() { ClientId = clientid, IsDirectLink = false/*?*/, MessageId = messageid, MessageIssueDate = DateTime.Now, TokenInfo = tokendata, Ip = ip }, json);
            return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        }

        public static async Task<string> GetDirectLinkData(Guid key)
        {
            cleanupDirectLinkData();
            if (directlinkDictionary.ContainsKey(key))
            {
                var result = directlinkDictionary[key];
                return result.Json;
            }
            else return await Task<string>.Run(() => { return Newtonsoft.Json.JsonConvert.SerializeObject(LocalizedString.Create(new Guid("618E4E72-7ADF-4FDB-BC84-E800B2E0188F"), "Data not found")); });
        }

        private static void AddDirectLink(Guid key, string json)
        {
            cleanupDirectLinkData();
            lock (padlock)
            {
                directlinkDictionary.Add(key, new DirectLinkInfo() { Json = json, Timestamp = DateTime.Now });
            }
        }

        private static readonly object padlock = new object();
        private static void cleanupDirectLinkData()
        {
            lock (padlock)
            {
                DateTime timeToLive = DateTime.Now.AddSeconds(60);
                List<Guid> ItemsToBeRemoved = new List<Guid>();

                foreach (var entry in directlinkDictionary)
                {
                    if (entry.Value.Timestamp > timeToLive)
                    {
                        ItemsToBeRemoved.Add(entry.Key);
                    }
                }
                foreach (var remove in ItemsToBeRemoved)
                {
                    directlinkDictionary.Remove(remove);
                }
            }
        }

        private static System.Collections.Generic.Dictionary<Guid, DirectLinkInfo> directlinkDictionary = new System.Collections.Generic.Dictionary<Guid, DirectLinkInfo>();
        public static void HandleMessage(string[] topicparts, ReturnMessage wrapper)
        {
            try
            {
                var context = Startup.signalRHub;
                int maxLength = 32000;
                string json = "";
                string ResponseToClientId = null;
                string ResponseToMessageId = null;

                if (topicparts[0] == "diagnostics")
                {
                    if (topicparts[1] == "trace")
                    {
                        json = System.Text.Encoding.UTF8.GetString(wrapper.Data);
                        ResponseToClientId = wrapper.ResponseToClientId;
                        ResponseToMessageId = wrapper.ResponseToMessageId;
                    }
                }
                else
                {
                    GatewayReturnMessage returnobj = new GatewayReturnMessage(wrapper);
                    returnobj.DataType = "json";
                    ResponseToClientId = wrapper.ResponseToClientId;
                    ResponseToMessageId = wrapper.ResponseToMessageId;
                    returnobj.Route = string.Join(".", topicparts);

                    json = Newtonsoft.Json.JsonConvert.SerializeObject(returnobj);
                }

                if (json.Length > maxLength)
                {
                    var id = Guid.NewGuid();
                    AddDirectLink(id, json);

                    GatewayReturnMessage re = new GatewayReturnMessage()
                    {
                        Message = LocalizedString.OK,
                        ResponseToMessageId = ResponseToMessageId,
                        Success = true,
                        Data = "https://auth.monosoft.dk/api/directlink/" + id.ToString(),
                        DataType = "url"
                    };
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(re);
                }
                if (context != null) {
                    if (string.IsNullOrEmpty(ResponseToClientId) == false)
                    {
                        context.Clients.Client(ResponseToClientId).SendAsync(
                            string.Join('.', topicparts),
                            json
                        ).Wait();
                    }
                    else
                    {
                        context.Clients.All.SendAsync(
                            string.Join('.', topicparts),
                            json
                        ).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                RPCController.LogTrace($"SignalR.Exception in Gatewayhub handler: {string.Join('.', topicparts)} exception: {ExceptionHelper.GetExceptionAsReportText(ex)}");
            }
        }
    }
}
